# JavaScript - Maps and Charts 

This project includes 3 map/chart web applications made with JavaScript

Exercise 1

 - This exercise is basic map integration using JavaScript. When you click one of the two areas it pop-ups a message. Application is build with JavaScript, HTML and CSS, map is build with Leaflet-library. You can find the working application from this url https://student.labranet.jamk.fi/~K9452/ttms0500-syksy2019-harjoitukset/Harjoitukset7/Teht%c3%a4v%c3%a4%201.html
 
 Exercise 2
 
  - This exercise is biger than the first one. Idea is to use ready libraries like Leaflet and google to build map to your application, then insert data from JSON-file using AJAX. Application prints golf courses from Finland and data about them. Techniques used in this is HTML, CSS, JavaScript, AJAX and JSON. You can find the working application from this url https://student.labranet.jamk.fi/~K9452/ttms0500-syksy2019-harjoitukset/Harjoitukset7/Teht%c3%a4v%c3%a4%202.html
  
  Exercise 3
  
   - This exercise is simple chart made with JavaScript. Chart is build with chart.js library. Techniques used in this is HTML, CSS and JavaScript. You can find the working application from this url https://student.labranet.jamk.fi/~K9452/ttms0500-syksy2019-harjoitukset/Harjoitukset7/Teht%c3%a4v%c3%a4%203.html